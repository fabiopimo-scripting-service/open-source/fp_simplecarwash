ESX.RegisterServerCallback("fp_carwash:startWashing", function(src, cb)
    local xPlayer = ESX.GetPlayerFromId(src)
    if xPlayer.getMoney() >= Config.Price then
        xPlayer.removeMoney(Config.Price)
        cb(true)
        return
    end
    cb(false)
end)
