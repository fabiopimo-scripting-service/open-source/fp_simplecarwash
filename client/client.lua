function getNearestWashingZoneDistance()
    local playerPos = GetEntityCoords(PlayerPedId())
    local currentNearestZone = nil
    local currentNearestDistance = math.huge

    for _, zone in pairs(Config.WashingZones) do
        local distance = Vdist(playerPos.x, playerPos.y, playerPos.z, zone.x, zone.y, zone.z)
        if distance < currentNearestDistance then
            currentNearestDistance = distance
            currentNearestZone = zone
        end
    end

    return currentNearestDistance, currentNearestZone
end

local nearestDistance, nearestZone = getNearestWashingZoneDistance();


function drawMarker(marker)
    DrawMarker(marker.type, marker.coords, vector3(0.0, 0.0, 0.0), marker.rotation or vector3(0.0, 0.0, 0.0),
        marker.scale,
        math.floor(marker.color.x), math.floor(marker.color.y), math.floor(marker.color.z),
        math.floor(marker.color.w), marker.bobUpAndDown or false, marker.faceCamera or false, 2, marker.rotate or false,
        nil, nil, false)
end

--create Blips
for _, info in pairs(Config.WashingZones) do
    -- create a blip in the middle
    local blip = AddBlipForCoord(info.x, info.y, info.z)
    SetBlipHighDetail(blip, true)
    SetBlipSprite(blip, info.sprite)
    SetBlipScale(blip, info.scale)
    SetBlipColour(blip, info.color_blip)
    SetBlipAsShortRange(blip, true)
    BeginTextCommandSetBlipName("STRING")
    AddTextComponentString(info.name)
    EndTextCommandSetBlipName(blip)
end


function enterNearWashingZone()
    isNearWashingZone = true
    Citizen.CreateThread(function()
        startNearWashingZoneTask()
        while isNearWashingZone and not (nearestZone == nil) do
            Citizen.Wait(0)
            drawMarker(nearestZone.marker)
        end
    end)
end

function startNearWashingZoneTask()
    Citizen.CreateThread(function()
        while (isNearWashingZone) do
            Citizen.Wait(0)
            showNotification(Translate("pressToStartWashing"):gsub('<PRICE>', Config.Price))
            if IsControlJustReleased(0, 38) then
                if not IsPedInAnyVehicle(PlayerPedId()) then
                    ESX.ShowNotification(Translate("cannotWashOutsideCar"), "error", Config.NotificationTime)
                    return
                end

                ESX.TriggerServerCallback('fp_carwash:startWashing', function(hasEnoughMoney)
                    if hasEnoughMoney then
                        VehicleWashed()
                    else
                        ESX.ShowNotification(Translate("hasNotEnoughMoney"), "error", Config.NotificationTime)
                    end
                end)
            end
        end
    end)
end

function VehicleWashed()
    local vehicle = GetVehiclePedIsIn(PlayerPedId(), false)
    if DoesEntityExist(vehicle) then
        SetVehicleDirtLevel(vehicle, 0)
        ESX.ShowNotification(Translate("VehicleWashed"), "success", Config.NotificationTime)
    end
end

function leaveNearWashingZone()
    isNearWashingZone = false
    ClearPedTasks(PlayerPedId())
end

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        nearestDistance, nearestZone = getNearestWashingZoneDistance();

        if not (nearestZone == nil) then
            if ((nearestZone.radius >= nearestDistance) and not isNearWashingZone) then
                enterNearWashingZone()
            else
                if (nearestZone.radius < nearestDistance) and isNearWashingZone then
                    leaveNearWashingZone()
                else
                    Citizen.Wait(1000)
                end
            end
        end
        Citizen.Wait(350)
    end
end)

function showNotification(message)
    SetTextComponentFormat('STRING')
    AddTextComponentString(message)
    EndTextCommandDisplayHelp(0, 0, 1, -1)
end
