Config = {}

Config.Locale = 'de'

Config.NotificationTime = 2000

Config.Price = 2000 --Price

Config.WashingZones = {
    -- Strawberry
    {
        x = 21.2019,
        y = -1391.8215,
        z = 29.3273,

        radius = 10.0,

        --Blip
        name = "Waschanlage",
        color_blip = 67,
        scale = 0.8,
        sprite = 100,
        display = 4,


        marker =
        {
            enabled = true,
            coords = vector3(21.2019, -1391.8215, 28.3273),
            scale = vector3(2.5, 2.5, 1.5),
            type = 1, -- https://docs.fivem.net/docs/game-references/markers/
            color = vector4(0, 255, 125, 125),
            rotation = vector3(0, 0, 0),
            bobUpAndDown = false,
            faceCamera = false,
            rotate = false
        },
    },
}
