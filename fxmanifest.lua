fx_version 'bodacious'
game 'gta5'

shared_script '@es_extended/imports.lua'

author 'FabioPimo'
description 'fp_carwash'
version '1.0.0'
lua54 'yes'

client_scripts {
    '@es_extended/locale.lua',
    'config.lua',
    'client/client.lua',
    'locales/*.lua',
}

server_scripts {
    '@es_extended/locale.lua',
    'server/server.lua',
    'config.lua',
    'locales/*.lua',
}

dependencies { "es_extended" }
