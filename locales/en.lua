Locales['en'] = {
    ['pressToStartWashing'] = 'Press ~g~E~s~ to wash your car, for <PRICE>€',
    ['cannotWashOutsideCar'] = '~r~You need a vehicle to wash it',
    ['hasNotEnoughMoney'] = '~r~You do not have enough money!',
    ['VehicleWashed'] = '~g~Your car is freshly washed!',
}
