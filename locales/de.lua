Locales['de'] = {
    ['pressToStartWashing'] = 'Drücke ~g~E~s~, um dein Auto, für <PRICE>€ zu waschen',
    ['cannotWashOutsideCar'] = '~r~Du brauchst ein Auto um es zu waschen',
    ['hasNotEnoughMoney'] = '~r~Du hast nicht genügend Geld!',
    ['VehicleWashed'] = '~g~Dein Auto ist frisch gewaschen!',
}
